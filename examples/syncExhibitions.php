<?php
require "../vendor/autoload.php";

use ReSourceAdapter\Adapter;

putenv("RESOURCE_ENDPOINT_URI=https://prod-resource-weever.mnemotix.com/graphql");

$resourceAdapter = new ReSourceAdapter\Adapter();
$resourceAdapter->setLang('fr');
$i=0;
$limit_expo = 0;
$window_size = 60;

try {
  do {
    echo("Fetching $window_size expositions from offset $limit_expo\n");
    $startFetch = time();

    $ressources = $resourceAdapter->{'findExhibitions'}('', $window_size, $limit_expo);
    $total = sizeof($ressources);

    foreach ($ressources as $ressource) {
      $ressource = json_decode(json_encode($ressource));
      $id = $ressource->id;
      $title = $ressource->title;
      echo("Fetching expo $id ($title)\n");
      $i++;

      // Batch Exposition fr
      $resourceAdapter->setLang('fr');
      $ressource_fr = $resourceAdapter->getExhibition($ressource->id);
      $ressource_fr = json_decode(json_encode($ressource_fr));

      echo("Fetched fr version\n");

      // Batch Exposition en
      $resourceAdapter->setLang('en');
      $ressource_en = $resourceAdapter->getExhibition($ressource->id);
      $ressource_en = json_decode(json_encode($ressource_en));

      echo("Fetched en version\n");

      $resourceAdapter->setLang('fr');
      $oeuvres = $resourceAdapter->{'findExhibitionArtworks'}($ressource->id, '', 50, NULL);

      echo("Fetching artworks\n");


      foreach ($oeuvres as $oeuvre) {
        $oeuvre = json_decode(json_encode($oeuvre));
        // Batch Oeuvre fr
        $id = $oeuvre->id;
        $title = $oeuvre->title;
        echo("Fetching artwork $id ($title)\n");

        $resourceAdapter->setLang('fr');
        $oeuvre_fr = $resourceAdapter->getArtwork($oeuvre->id);
        $oeuvre_fr = json_decode(json_encode($oeuvre_fr));
        // Batch Oeuvre en
        $resourceAdapter->setLang('en');
        $oeuvre_en = $resourceAdapter->getArtwork($oeuvre->id);
        $oeuvre_en = json_decode(json_encode($oeuvre_en));
        usleep(500000);
      }
    }

    $limit_expo += $window_size;
    $duration = time() - $startFetch;
    echo("Done in $duration seconds\n\n");
  } while ($total > 0);
} catch(Exception $e) { // catch the throws in the catch-block
  echo "Error !\n";
  echo($e);
}