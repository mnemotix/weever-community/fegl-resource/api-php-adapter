<?php
/**
 * This file is part of the re-source-drupal-adapter package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 11/05/2017
 */
require "../vendor/autoload.php";

use ReSourceAdapter\Adapter;

$adapter = new Adapter();

$qs = "";
$first = 10;
$after = null;

if (isset($_GET["date"])) {
  $date = $_GET["date"];
}

$actors = $adapter->getResourcesDifferentialSince($date);

header("Content-type:application/json");
echo json_encode($actors, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
