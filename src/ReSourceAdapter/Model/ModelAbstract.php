<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\IdHelper;

abstract class ModelAbstract implements \JsonSerializable {
  /** @var $id */
  protected $id;
  /** @var $id */
  protected $seeAlso;
  /** @var $creationDate */
  protected $creationDate;
  /** @var $lastUpdate */
  protected $lastUpdate;

  /**
   * @return mixed
   */
  public function getId() {
    if ($this->seeAlso){
      $seeAlso = json_decode($this->seeAlso);
      if(isset($seeAlso->legacyId)){
        return IdHelper::toOldId($seeAlso->legacyId);
      }
    }
    return $this->id;
  }

  public function getCreationDate() {
    if ($this->creationDate) {
      return date("U000",strtotime($this->creationDate));
    }
  }

  public function getLastUpdate() {
    if ($this->lastUpdate) {
      return date("U000",strtotime($this->lastUpdate));
    }

    return $this->getCreationDate();
  }

  public function getSeeAlso() {
    return $this->seeAlso;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate()
    ];
  }
}
