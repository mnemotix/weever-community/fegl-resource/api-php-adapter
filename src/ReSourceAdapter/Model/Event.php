<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;
use ReSourceAdapter\Helpers\ListQueryParams;

class Event extends ModelAbstract {
  /** @var string Event title */
  protected $title;

  /** @var string Event description */
  protected $description;

  /** @var int Event startDate */
  protected $startDate;

  /** @var int Event endDate */
  protected $endDate;

  /** @var int Is event highlighted */
  protected $isHighlighted;

  /** @var int Event location */
  protected $location;

  /** @var \ReSourceAdapter\Model\Involvement[] Event involvements */
  protected $involvements;

  /** @var \ReSourceAdapter\Model\Resource[] Event Resources */
  protected $resources;

  /** @var \ReSourceAdapter\Model\Memo[] Event Memos */
  protected $memos;

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return int
   */
  public function getStartDate() {
    if ($this->startDate){
      return  date("U000",strtotime($this->startDate));
    }
  }

  /**
   * @return int
   */
  public function getEndDate() {
    if ($this->endDate){
      return  date("U000",strtotime($this->endDate));
    }
  }

  /**
   * @return int
   */
  public function getLocation() {
    return $this->location;
  }

  /**
   * @return \ReSourceAdapter\Model\Involvement[]
   */
  public function getInvolvements() {
    return $this->involvements;
  }

  /**
   * @return \ReSourceAdapter\Model\Resource[]
   */
  public function getResources() {
    return $this->resources;
  }

  /**
   * @return \ReSourceAdapter\Model\Memo[]
   */
  public function getMemos() {
    return $this->memos;
  }

  /**
   * @return int
   */
  public function getIsHighlighted() {
    return $this->isHighlighted;
  }
  
  /**
   * Get event GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $eventInvolvementFragmentName = Fragment::generateName();
    $eventInvolvementFragment = Involvement::getFragment($eventInvolvementFragmentName);

    $resourceFragmentName = Fragment::generateName();
    $resourceFragment = Resource::getFragment($resourceFragmentName);

    $memoFragmentName = Fragment::generateName();
    $memoFragment = Memo::getFragment($memoFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on ProjectContribution{
  id
  title
  description
  endDate
  startDate
  seeAlso 
  creationDate: createdAt
  lastUpdate: updatedAt
  involvements(first: 100){
    edges{
      involvement: node{
        ...$eventInvolvementFragmentName
      }
    }
  }
  attachments(first: 100){
    edges{
      attachment: node{
        resource{
          ...$resourceFragmentName
        }
      }
    }
  }
  memos: replies(first: 100){
    edges{
      memo: node{
        ...$memoFragmentName
      }
    }
  }
}

$eventInvolvementFragment
$resourceFragment
$memoFragment

GRAPHQL;
  }

  /**
   * Get event GraphQL query
   * @param $eventId
   * @return string
   */
  static function getQuery($eventId){
    $fragmentName = Fragment::generateName();
    $fragment = self::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  event: projectContribution(id:"$eventId") {
    ...$fragmentName
  }
}

$fragment
GRAPHQL;
  }

  /**
   * Get event from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Event
   */
  static function fromResponse($data) {
    $data = $data['event'];

    $event = new Event();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'attachments':
          $event->resources = [];

          if(isset($value)) {
            foreach ($value['edges'] as $attachmentNode){
              $event->resources[] = Resource::fromResponse($attachmentNode['attachment']);
            }
          }
          break;
        case 'involvements':
          $event->involvements = [];
          if(isset($value)) {
            foreach ($value['edges'] as $actorNode){
              $event->involvements[] = Involvement::fromResponse($actorNode);
            }
          }
          break;
        case 'memos':
          $event->memos = [];
          if(isset($value)) {
            foreach ($value['edges'] as $memoNode){
              $event->memos[] = Memo::fromResponse($memoNode);
            }
          }
          break;
        default:
          $event->{$property} = $value;
      }
    }

    return $event;
  }

  /**
   * Get artistic object related project events list GraphQL query.
   *
   * @param $artisticObjectId
   * @param \ReSourceAdapter\Helpers\ListQueryParams $args
   * @return string
   */
  static function getListQuery($artisticObjectId, ListQueryParams $args){
    $fragmentName = Fragment::generateName();
    $fragment = Event::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  exhibition(id: "$artisticObjectId") {
    projects{
      edges{
       node{
         events: projectContributions({$args->graphQLize()}){
           edges{
             event: node{
               ...$fragmentName
             }
           }
         }
       }
      }

    }
  }
}

$fragment

GRAPHQL;
  }

  /**
   * Return a list of artworks from a GraphQL response.
   *
   * @param array $data
   * @return \ReSourceAdapter\Model\Event[]
   */
  static function fromListResponse(array $data){
    $events = [];

    foreach ($data['exhibition']['projects']['edges'] as $projectsData) {
      foreach ($projectsData['node']['events']['edges'] as $eventsData) {
        $events[] = Event::fromResponse($eventsData);
      }
    }

    return $events;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
      'startDate' => $this->getStartDate(),
      'endDate' => $this->getEndDate(),
      'location' => $this->getLocation(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate(),
      'involvements' => array_map(function($involvement){return $involvement->jsonSerialize(); }, $this->getInvolvements()),
      'resources' => array_map(function($resource){return $resource->jsonSerialize(); }, $this->getResources()),
      'memos' => array_map(function($memo){return $memo->jsonSerialize(); }, $this->getMemos()),
      'isHighlighted' => $this->getIsHighlighted()
    ];
  }
}
