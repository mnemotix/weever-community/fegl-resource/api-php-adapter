<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;
use ReSourceAdapter\Helpers\ListQueryParams;

class ResourceActivity extends ModelAbstract {
  /** @var \ReSourceAdapter\Model\Resource Resource */
  protected $resource;

  /** @var string Action among the list CREATED, DELETED, UPDATED */
  protected $action;

  /**
   * @return \ReSourceAdapter\Model\Resource
   */
  public function getResource() {
    return $this->resource;
  }

  /**
   * @return string
   */
  public function getAction() {
    return 'CREATED'; //$this->action;
  }

  /**
   * Get resource GraphQL query
   *
   * @param \ReSourceAdapter\Helpers\ListQueryParams $args
   * @return string
   */
  static function getListQuery(ListQueryParams $args){
    $resourceFragmentName = Fragment::generateName();
    $resourceFragment = Resource::getFragment($resourceFragmentName);

    return <<<GRAPHQL
query{
  resourcesDiff: files({$args->graphQLize()}) {
    edges{
      resource: node {
        ...$resourceFragmentName
      }
    }
  }
}

$resourceFragment
GRAPHQL;
  }

  /**
   * Return a list of artworks from a GraphQL response.
   *
   * @param array $data
   * @return \ReSourceAdapter\Model\ResourceActivity[]
   */
  static function fromListResponse(array $data){
    $resourceActivities = [];

    foreach ($data['resourcesDiff']['edges'] as $resourceData) {
      $resourceActivities[] = self::fromResponse($resourceData);
    }

    return $resourceActivities;
  }

  /**
   * Get event from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\ResourceActivity
   */
  static function fromResponse($resourceData) {

    $resourceActivity = new ResourceActivity();
    $resourceActivity->resource = Resource::fromResponse($resourceData);

    return $resourceActivity;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'resource' => $this->getResource()->jsonSerialize(),
      'action' => $this->getAction()
    ];
  }
}
