<?php
/**
 * This file is part of the api-php-adapter package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/09/2020
 */

namespace ReSourceAdapter\Helpers;

class IdHelper {
  public static function toNewId($id){
    // Test if id is base64 encoded. If true, this is an old id we need
    // to convert.
    if (strpos($id, "/") === false && strpos($id, "%2F") === false){
      $oldId = base64_decode($id);
      $dividerPos = strpos($oldId, ":", 0);
      $type = substr($oldId, 0, $dividerPos );

      $typeMapping = [
        "Person" => 'person',
        "Organisation" => 'organization',
        "ExhibitionObject" => 'exhibition',
        "ArtworkObject" => 'artwork',
        "Exhibition" => 'exhibition',
        "Artwork" => 'artwork',
        "Event" => 'project-contribution',
        "Memo" => 'project-contribution',
        "Resource" => 'resource'
      ];

      if(isset($typeMapping[$type])){
        $newType = $typeMapping[$type];
      } else {
        $newType = strtolower($type);
      }

      $oldId = substr($oldId, $dividerPos + 1);

      $newId = preg_replace("/\w+\:(\d+)\:(\d+)/", '${1}${2}', $oldId);
      $id = "$newType/$newId";
    }

    return $id;
  }

  public static function toOldId($id){
    // Test if id is base64 encoded. If true, this is an old id we need
    // to convert.
    if (strpos($id, "/") === false && strpos($id, "%2F") === false){
      $dividerPos = strpos($id, ":", 0);
      $type = substr($id, 0, $dividerPos );

      $typeMapping = [
        "Exhibition" => "ExhibitionObject",
        "Artwork" => 'ArtworkObject',
      ];

      if(isset($typeMapping[$type])){
        $newType = $typeMapping[$type];
      } else {
        $newType =$type;
      }

      $id = base64_encode("$newType:$id");
    }

    return $id;
  }
}
