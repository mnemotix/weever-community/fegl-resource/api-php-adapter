<?php
/**
 * This file is part of the lafayette-anticipations package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2017
 */
error_reporting(E_ALL | E_STRICT);

putenv("RESOURCE_ENDPOINT_URI=http://localhost:3004/api");
putenv("RESOURCE_AUTH_URI=http://localhost:8181/auth/realms/synaptix/protocol/openid-connect/token");
putenv("RESOURCE_API_KEY=ZHJ1cGFsOjpmMTVlMWYwZi1kNDBmLTQ3ZjItYTc5OS05MjlmMmE2Nzg2YWQ6OmRydXBhbC1hZGFwdGVyOjo1UGJVeUltRm44TzE=");

// Set the default timezone. While this doesn't cause any tests to fail, PHP
// complains if it is not set in 'date.timezone' of php.ini.
date_default_timezone_set('UTC');

// Ensure that composer has installed all dependencies
if (!file_exists(dirname(__DIR__) . '/composer.lock')) {
  die("Dependencies must be installed using composer:\n\nphp composer.phar install --dev\n\n"
    . "See http://getcomposer.org for help with installing composer\n");
}

// Include the composer autoloader
$autoloader = require_once(dirname(__DIR__) . '/vendor/autoload.php');
